package domain

type PointData struct {
	Lat float32
	Lon float32
}
