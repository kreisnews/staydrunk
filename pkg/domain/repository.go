package domain

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"image"
	"image/draw"
	"image/jpeg"
	"image/png"
	"io/ioutil"
	"math"
	"net/http"
	"net/url"
	"os"
)

var internalError = status.Errorf(codes.Internal, "something went wrong")
var pp []Point

const (
	appID = "VTSCRCeJm7bi6SsFK9Pa"
	appCode = "LX7ctV3WO2z0hhOyQs_9Rg"
)

type Repository interface {
	AssertPlace(ctx context.Context, req *Point) (*Empty, error)
	RequestDirection(ctx context.Context, req *Point) (*Image, error)
}

type repository struct {
	logger *logrus.Entry
	points []Point
}

func (r repository) AssertPlace(ctx context.Context, req *Point) (*Empty, error) {
	//r.points = append(r.points, *req)
	pp = append(pp, *req)

	r.logger.Infof("Got new point: %f, %f", req.Lat, req.Lon)

	res := &Empty{}

	r.logger.Info(len(pp))

	return res, nil
}

func (r repository) RequestDirection(ctx context.Context, req *Point) (*Image, error) {
	r.logger.Info(len(pp))

	if len(pp) == 0 {
		return nil, status.Errorf(codes.FailedPrecondition, "no points")
	}

	min := pp[0].greatCircleDistance(req)
	nearest := Point{}

	for _, p := range pp {
		if p.greatCircleDistance(req) < min {
			min = p.greatCircleDistance(req)
			nearest = p
		}
	}

	r.logger.Infof("Find nearest point: %f,%f", nearest.Lat, nearest.Lon)

	name := uuid.New().String() + ".jpeg"

	err := processMap(r.logger, nearest.Lat, nearest.Lon, name)
	if err != nil {
		return nil, internalError
	}

	res := &Image{
		Src: "http://substancial.ru/" + name,
	}

	return res, nil
}

func NewRepository(logger *logrus.Entry, points []Point) Repository {
	return &repository{
		logger: logger,
		points: points,
	}
}

func (p *Point) greatCircleDistance(p2 *Point) float64 {
	dLat := float64((p2.Lat - p.Lat) * (math.Pi / 180.0))
	dLon := float64((p2.Lon - p.Lon) * (math.Pi / 180.0))

	lat1 := p.Lat * (math.Pi / 180.0)
	lat2 := p2.Lat * (math.Pi / 180.0)

	a1 := math.Sin(dLat/2) * math.Sin(dLat/2)
	a2 := math.Sin(dLon/2) * math.Sin(dLon/2) * math.Cos(float64(lat1)) * math.Cos(float64(lat2))

	a := a1 + a2

	c := 2 * math.Atan2(math.Sqrt(a), math.Sqrt(1-a))

	return 6371. * c
}

func processMap(logger *logrus.Entry, lat, lon float32, name string) error {
	name = "data/" + name

		ep, _ := url.Parse("https://image.maps.api.here.com/mia/1.6/mapview")
	queryParams := ep.Query()
	queryParams.Set("app_id", appID)
	queryParams.Set("app_code", appCode)
	queryParams.Set("w", "1280")
	queryParams.Set("h", "1920")
	queryParams.Set("z", "17")
	queryParams.Set("c", fmt.Sprintf("%f,%f", lat, lon))

	ep.RawQuery = queryParams.Encode()
	response, err := http.Get(ep.String())
	if err != nil {
		logger.Errorf("Req err: %v", err)
		return err
	} else {
		f, err := os.Create(name)
		if err != nil {
			logger.Errorf("failed to create mapview file: %v", err)
			return err
		}
		defer f.Close()
		data, _ := ioutil.ReadAll(response.Body)
		f.Write(data)
	}

	mapview, err := os.Open(name)
	if err != nil {
		logger.Errorf("failed to open mapview: %v", err)
		return err
	}
	defer mapview.Close()

	mapviewDecoded, err := jpeg.Decode(mapview)
	if err != nil {
		logger.Errorf("failed to decode mapview: %v", err)
		return err
	}

	point, err := os.Open("data/point.png")
	if err != nil {
		logger.Errorf("failed to open point: %v", err)
		return err
	}
	defer point.Close()

	pointDecoded, err := png.Decode(point)
	if err != nil {
		logger.Errorf("failed to decode point: %v", err)
		return err
	}

	offset := image.Pt(mapviewDecoded.Bounds().Size().X / 2 - pointDecoded.Bounds().Size().X / 2, mapviewDecoded.Bounds().Size().Y / 2 - pointDecoded.Bounds().Size().Y)
	b := mapviewDecoded.Bounds()

	output := image.NewRGBA(b)
	draw.Draw(output, b, mapviewDecoded, image.ZP, draw.Src)
	draw.Draw(output, pointDecoded.Bounds().Add(offset), pointDecoded, image.ZP, draw.Over)

	merged, err := os.Create(name)
	if err != nil {
		logger.Errorf("failed to create output file: %v", err)
		return err
	}
	defer merged.Close()

	jpeg.Encode(merged, output, &jpeg.Options{jpeg.DefaultQuality})

	return nil
}
