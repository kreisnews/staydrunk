package main

import (
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"net"
	"staydrunk/pkg/domain"
)

var gitCommit string

func main() {
	var logger = logrus.WithFields(logrus.Fields{"commit": gitCommit})
	logger.Info("App is starting")

	var points []domain.Point

	rep := domain.NewRepository(logger, points)

	l, err := net.Listen("tcp", "0.0.0.0:8080")
	if err != nil {
		logger.Fatalf("failed to listen")
	}

	grpcServer := grpc.NewServer()

	domain.RegisterStayDrunkServer(grpcServer, rep)

	logger.Infof("gRPC server is listening on: %s", "0.0.0.0:8080")
	err = grpcServer.Serve(l)

	if err != nil {
		logger.Fatalf("serve error: %v", err)
	}
}
